<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de libro</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<form action="create-libro.php" method="post">
<table>
  <caption>Información de libro</caption>
  <tbody>
    <tr>
      <th>ISBN</th>
      <td><input type="text" name="isbn" /></td>
    </tr>
    <tr>
      <th>Titulo</th>
      <td><textarea name="titulo_libro"></textarea></td>
    </tr>
  </tbody>
</table>
<input type="submit" name="submit" value="CREATE" />
</form>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
</ul>

</body>
</html>